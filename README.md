# OpenNBT for WorldSynth
This is a fork of **[OpenNBT](https://github.com/Steveice10/OpenNBT)** by Steveice10 over on github with modification by the WorldSynth team for use with WorldSynth.

# OpenNBT
OpenNBT is a library for reading and writing NBT files, with some extra custom tags added to allow the storage of more data types.

## Building the Source
OpenNBT for WorldSynth uses Gradle to manage building and dependencies. Simply run './gradle eclipse' in the source's directory to setup the project for Eclipse.

## License
OpenNBT is licensed under the **[MIT license](http://www.opensource.org/licenses/mit-license.html)**.
